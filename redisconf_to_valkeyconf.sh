#!/bin/sh
cp /etc/redis/redis.conf /etc/valkey/valkey.conf
cp /etc/redis/sentinel.conf /etc/valkey/sentinel.conf
mv /var/lib/redis/* /var/lib/valkey/
echo "/etc/redis/redis.conf has been copied to /etc/valkey/valkey.conf.  pidfile, logfile, and dir are overridden by the config at /etc/sysconfig/valkey.  Manual review of valkey.conf is strongly suggested especially if you had modified redis.conf."
echo "/etc/redis/sentinel.conf has been copied to /etc/valkey/sentinel.conf.  pidfile, logfile, and dir are overridden by the config at /etc/sysconfig/valkey-sentinel.  Manual review of sentinel.conf is strongly suggested especially if you had modified sentinel.conf."
echo "On-disk redis dumps moved from /var/lib/redis/ to /var/lib/valkey"
